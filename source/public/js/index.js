$(document).ready(function(){
	console.log('document ready to go!');
	var bullets,
	bulletArray;

	// update output on keyup
	$('#bullets').on('keyup', function(e) {
		bullets = $('#bullets').html();

		// get first element (which does not get wrapped in div)
		var firstBullet = $("#bullets")
		    .clone()    //clone the element
		    .children() //select all the children
		    .remove()   //remove all the children
		    .end()  //again go back to selected element
		    .text();

		// get array of text values of all divs within the bullets textbox
		bulletArray = $('#bullets div').map(function(){
			return $.trim($(this).text());
		}).get();

		// add first bullet to beginning of bullet array
		bulletArray.unshift(firstBullet);
		console.log(bulletArray);

		// output bullets after changes have been made
		$('#output').html(bullets);
	});
});